﻿using System.Collections.Generic;
using System.Linq;

namespace day1
{
    public static class Window
    {
        public static IEnumerable<IEnumerable<T>> ToSlidingWindow<T>(this IEnumerable<T> list, int windowSize)
        {
            foreach (var (item, index) in list.Select((item, index) => (item, index)))
            {
                yield return list.Skip(index).Take(windowSize);
            }
        }

    }
}