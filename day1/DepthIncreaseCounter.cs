﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day1
{
    public class IncreaseCounter
    {
        IEnumerable<int> measurements;
        public IncreaseCounter(IEnumerable<int> measurements)
        {
            this.measurements = measurements;
        }

        public int CountIncreases() {
            int increases = 0;
            int? lastDepth = null;
            foreach (var depth in measurements) {
                if (lastDepth != null) {
                    if (depth > lastDepth) {
                        increases += 1;
                    }
                }
                lastDepth = depth;
            }

            return increases;
        }
    }
    public class DepthIncreaseCounter
    {
        IncreaseCounter counter;
        public DepthIncreaseCounter(IEnumerable<DepthMeasurement> measurements)
        {
            counter = new IncreaseCounter(measurements.Select(x => x.Depth));
        }

        public int CountIncreases()
        {
            return this.counter.CountIncreases();
        }
    }
}