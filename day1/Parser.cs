﻿using System.Collections.Generic;
using System.Linq;
using Shared;

namespace day1
{
    public record DepthMeasurement
    {
        public DepthMeasurement(int depth)
        {
            Depth = depth;
        }

        public int Depth { init; get; }
    }

    public static class DepthParser
    {
        public static IEnumerable<DepthMeasurement> parse(string path)
        {
            return Parser.parse(path).Select(line => new DepthMeasurement(int.Parse(line)));
        }
    }
}