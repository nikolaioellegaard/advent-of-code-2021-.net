﻿using System;
using System.Linq;
using day1;

var input = DepthParser.parse("input.txt");
var counter = new DepthIncreaseCounter(input);
Console.WriteLine($"How many measurements are larger than the previous measurement? {counter.CountIncreases()}");

var windowedInput = input
    .ToSlidingWindow(3)
    .Select(window => window.Select(measurement => measurement.Depth).Sum());

var windowedCounter = new IncreaseCounter(windowedInput);

Console.WriteLine($"How many sums are larger than the previous sum? {windowedCounter.CountIncreases()}");
