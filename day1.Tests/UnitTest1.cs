using Xunit;
using day1;

namespace day1.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(7, new DepthIncreaseCounter(new DepthMeasurement[]
            {
                new (199),
                new (200),
                new (208),
                new (210),
                new (200),
                new (207),
                new (240),
                new (269),
                new (260),
                new (263),
            }).CountIncreases());
        }
    }
}